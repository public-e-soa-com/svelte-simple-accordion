# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [1.1.0](https://gitlab.com/public-e-soa-com/svelte-simple-accordion) (2021-10-26)

Added the sub-accordion feature.  
Fixed an issue with the random ID of a DOM element.  
Fixed typo and indentations.

# [1.0.0](https://gitlab.com/public-e-soa-com/svelte-simple-accordion) (2021-07-02)

First commit
