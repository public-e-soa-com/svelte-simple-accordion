/* Copyright (c) 2021 Jacques Desodt */
// noinspection JSUnusedGlobalSymbols

import css from 'rollup-plugin-css-only';
import pkg from './package.json';
import resolve from '@rollup/plugin-node-resolve';
import svelte from 'rollup-plugin-svelte';

const name = pkg.name
    .replace(/^(@\S+\/)?(svelte-)?(\S+)/, '$3')
    .replace(/^\w/, m => m.toUpperCase())
    .replace(/-\w/g, m => m[1].toUpperCase());

export default {
    input: 'src/index.js',
    output: [
        {file: pkg.module, 'format': 'es'},
        {file: pkg.main, 'format': 'umd', name},
    ],
    plugins: [
        css({ output: 'bundle.css' }),
        resolve(),
        svelte(),
    ],
};
